FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/woop.sh"]

COPY woop.sh /usr/bin/woop.sh
COPY target/woop.jar /usr/share/woop/woop.jar
